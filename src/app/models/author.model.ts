export interface Author{
    id:Int32Array,
    name:string,
    biography:string,
    imageUrl:string
}