export interface Book{
    id:Int32Array,
    name:string,
    description:string,
    type:string,
    isbn:string,
    author:string,
    imageUrl:string
}