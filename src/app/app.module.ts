import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { HomePageComponent } from './components/home-page/home-page.component';
import { Routes } from '@angular/router';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { NavigationComponent } from './components/navigation/navigation.component';
import { AddAuthorPageComponent } from './components/addauthor-page/addauthor-page.component';
import { FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from '@angular/common/http';
import {FlexLayoutModule} from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import { DisplayAuthorsPageComponent } from './components/display-authors-page/display-authors-page.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { AddBookPageComponent } from './components/addbook-page/addbook-page.component';
import { BookShowComponent } from './components/book-show/book-show.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSelectModule} from '@angular/material/select';
import { MatNativeDateModule } from '@angular/material/core';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { DisplayBooksPageComponent } from './components/display-books-page/display-books-page.component';
import { FooterComponent } from './components/footer/footer.component';
import { AuthorShowComponent } from './components/author-show/author-show.component';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent
  },
  {
    path: 'addAuthor',
    component: AddAuthorPageComponent
  },
  {
    path: 'addBook',
    component: AddBookPageComponent
  },
  {
    path: 'addBookShow',
    component: BookShowComponent
  },
  {
    path: 'allAuthor',
    component: DisplayAuthorsPageComponent
  },
  {
    path: 'allBook',
    component: DisplayBooksPageComponent
  },
  { 
    path: 'bookDetail/:id', 
    component: BookShowComponent 
  },
  { 
    path: 'authorDetail/:id', 
    component: AuthorShowComponent 
  },
  { 
    path: 'authorDetail/:name', 
    component: AuthorShowComponent 
  },
  { 
    path: 'displayAuthorDetail/:name', 
    component: AuthorShowComponent 
  }

  
];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    NavigationComponent,
    FooterComponent,
    BookShowComponent,
    AddAuthorPageComponent,
    AddBookPageComponent,
    DisplayAuthorsPageComponent,
    DisplayBooksPageComponent,
    AuthorShowComponent,
  ],

  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MatToolbarModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    MatInputModule,
    MatButtonToggleModule,
    MatButtonModule,
    MatSnackBarModule,
    MatMenuModule,
    FormsModule,
    MatCardModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    FlexLayoutModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
