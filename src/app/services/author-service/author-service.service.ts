import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, tap, throwError } from 'rxjs';
import { Author } from 'src/app/models/author.model';
import { baseUrl } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AuthorServiceService {

  constructor(private http:HttpClient) { }

  getAuthors():Observable<Author[]>{
    return this.http.get<Author[]>(`${baseUrl}/api/author/get`);
  }
  getAuthorByName(name:any){
    return this.http.get<Author>(`${baseUrl}/api/author/getByName/${name}`,name);
  }
  addAuthor(data:any){
    return this.http.post(`${baseUrl}/api/author/create`,data);
  }
  getLastAuthors():Observable<Author[]>{
    return this.http.get<Author[]>(`${baseUrl}/api/author/getLastAuthors`);
  }
  getAuthorById(id:any):Observable<Author>{
    return this.http.get<Author>(`${baseUrl}/api/author/getById?id=${id}`);
  }

}