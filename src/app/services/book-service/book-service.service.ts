import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { baseUrl } from 'src/environments/environment';
import { catchError, Observable, tap, throwError } from 'rxjs';
import { Book } from 'src/app/models/book.model';
@Injectable({
  providedIn: 'root'
})
export class BookServiceService {

  constructor(private http:HttpClient) { }
  addBook(data:any){
    return this.http.post(`${baseUrl}/api/book/create`,data);
  }
  getBooks():Observable<Book[]>{
    return this.http.get<Book[]>(`${baseUrl}/api/book/get`);
  }
  getLastBooks():Observable<Book[]>{
    return this.http.get<Book[]>(`${baseUrl}/api/book/getLastBooks`); 
  }
  getBookById(id:any):Observable<Book>{
    return this.http.get<Book>(`${baseUrl}/api/book/getById?id=${id}`);
  }
  getBookByAuthorName(name:any):Observable<Book[]>{
    return this.http.get<Book[]>(`${baseUrl}/api/book/getByAuthorName?name=${name}`);
  }
}