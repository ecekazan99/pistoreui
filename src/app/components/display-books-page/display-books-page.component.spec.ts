import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayBooksPageComponent } from './display-books-page.component';

describe('DisplayBooksPageComponent', () => {
  let component: DisplayBooksPageComponent;
  let fixture: ComponentFixture<DisplayBooksPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayBooksPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayBooksPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
