import { Component, OnInit } from '@angular/core';
import { BookServiceService } from 'src/app/services/book-service/book-service.service';
import { Book } from 'src/app/models/book.model';
import {Router} from "@angular/router";
@Component({
  selector: 'app-display-books-page',
  templateUrl: './display-books-page.component.html',
  styleUrls: ['./display-books-page.component.scss']
})
export class DisplayBooksPageComponent implements OnInit {

  constructor(private getBookService:BookServiceService,private routes: Router) { }
  bookArray:Book[]=[];
  
  ngOnInit() {
      this.getBookService.getBooks().subscribe((data:any[])=>{
      this.bookArray=data;
    });
  }  
  
  displayBookDetail(id:any){
    this.routes.navigate(['/bookDetail',id]);
  }
}
