import { Component, OnInit } from '@angular/core';
import { BookServiceService } from 'src/app/services/book-service/book-service.service';
import { AuthorServiceService } from 'src/app/services/author-service/author-service.service';
import {Router} from "@angular/router";
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor(private getBookService: BookServiceService, private getAuthorService: AuthorServiceService,private routes: Router) { }
  bookArray:any=[];
  authorArray:any=[];

  ngOnInit() {
    this.getBookService.getLastBooks().subscribe((data: any[]) => {
      this.bookArray = data;
    });
    this.getAuthorService.getLastAuthors().subscribe((data:any[])=>{
      this.authorArray=data;
    });
  }
  displayBookDetail(id: any) {

    this.routes.navigate(['/bookDetail',id]);
  }

  displayAuthorDetail(id:any){
    this.routes.navigate(['/authorDetail',id]);
  } 
}
