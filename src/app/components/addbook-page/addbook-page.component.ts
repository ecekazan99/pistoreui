import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BookServiceService } from 'src/app/services/book-service/book-service.service';
import { AuthorServiceService } from 'src/app/services/author-service/author-service.service';
import {Router} from "@angular/router";
@Component({
  selector: 'app-addbook-page',
  templateUrl: './addbook-page.component.html',
  styleUrls: ['./addbook-page.component.scss']
})
export class AddBookPageComponent implements OnInit {

  addBookFormGroup!:FormGroup;
  constructor(private bookService:BookServiceService,private authorService:AuthorServiceService,private routes: Router) { }
  authorArray:any=[];

  ngOnInit(): void { 
    this.addBookFormGroup=new FormGroup({
      name:new FormControl('',[Validators.required]),
      type:new FormControl('',[Validators.required]),
      authorName:new FormControl('',[Validators.required]),
      isbn:new FormControl('',[Validators.required]),
      imageUrl:new FormControl('',[Validators.required]),
      description:new FormControl('',[Validators.required])
    });

    this.authorService.getAuthors().subscribe((data:any[])=>{
      this.authorArray=data;
     });
  
  }

  handleSubmit(data:any){
    if (this.addBookFormGroup.valid){
          this.bookService.addBook(data).subscribe(result=>{
          alert("New book successfully registered !! ");
          this.routes.navigate(['/allBook']);
        });
    }
  }
}