import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute } from "@angular/router";
import { Book } from 'src/app/models/book.model';
import { BookServiceService } from 'src/app/services/book-service/book-service.service';
@Component({
  selector: 'app-book-show',
  templateUrl: './book-show.component.html',
  styleUrls: ['./book-show.component.scss']
})
export class BookShowComponent implements OnInit {

  constructor(private routes: Router,private _Activatedroute:ActivatedRoute,private getBookService: BookServiceService) { }
  id:any;
  book!:Book;
 
  ngOnInit(): void {
    this.id=this._Activatedroute.snapshot.paramMap.get("id");
  
    this.getBookService.getBookById(this.id).subscribe((data: any) => {
      this.book = data;
    });
  }

  displayAuthorDetail(authorName:any){
    this.routes.navigate(['/displayAuthorDetail',authorName]);
    
  }

}
