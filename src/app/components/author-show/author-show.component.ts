import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute } from "@angular/router";
import { Author } from 'src/app/models/author.model';
import { AuthorServiceService } from 'src/app/services/author-service/author-service.service';
import { BookServiceService } from 'src/app/services/book-service/book-service.service';

@Component({
  selector: 'app-author-show',
  templateUrl: './author-show.component.html',
  styleUrls: ['./author-show.component.scss']
})
export class AuthorShowComponent implements OnInit {

  constructor(private routes: Router,private _Activatedroute:ActivatedRoute,private getAuthorService: AuthorServiceService, private getBookService: BookServiceService) { }
  id:any;
  author!: Author;
  authorName:any;
  bookArray:any=[];

  ngOnInit(): void {
    this.id = this._Activatedroute.snapshot.paramMap.get("id");
    this.authorName = this._Activatedroute.snapshot.paramMap.get("name");
   
    if(this.id!=null){
       this.getAuthorService.getAuthorById(this.id).subscribe((data: any) => {
      this.author = data;
      this.getBookService.getBookByAuthorName(data.name ).subscribe((data: any[]) => {
        this.bookArray = data;
      });
    });
    }
    else{
      this.getAuthorService.getAuthorByName(this.authorName).subscribe((data: any) => {
        this.author = data;
        this.getBookService.getBookByAuthorName(data.name ).subscribe((data: any[]) => {
          this.bookArray = data;
        });
      });
    }
    
   console.log( this.author.name);
    
  }

  displayAuthorDetail(id:any){
    this.routes.navigate(['/authorDetail',id]);
  } 
  displayBookDetail(id:any){
    this.routes.navigate(['/bookDetail',id]);
  }
}
