import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAuthorPageComponent } from './addauthor-page.component';

describe('AddauthorPageComponent', () => {
  let component: AddAuthorPageComponent;
  let fixture: ComponentFixture<AddAuthorPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAuthorPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAuthorPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
