import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthorServiceService } from 'src/app/services/author-service/author-service.service';
import {Router} from "@angular/router";
@Component({
  selector: 'app-addauthor-page',
  templateUrl: './addauthor-page.component.html',
  styleUrls: ['./addauthor-page.component.scss']
})
export class AddAuthorPageComponent implements OnInit {

  addAuthorFormGroup!:FormGroup;

  constructor(private authorService:AuthorServiceService,private routes: Router) { }



  ngOnInit(): void { this.addAuthorFormGroup=new FormGroup({
      name:new FormControl('',[Validators.required]),
      imageUrl:new FormControl('',[Validators.required]),
      biography:new FormControl('',[Validators.required])
    });
  }
  handleSubmit(data:any){
     
    if (this.addAuthorFormGroup.valid){
     
      this.authorService.addAuthor(data).subscribe(result=>{
        
         alert("New author successfully registered !! ");
         this.routes.navigate(['']);
        
      },error=>{
        alert("There is an author with the name "+data.name+" !!!");
        this.routes.navigate(['/addAuthor']);
      });
    
    }
  }
}