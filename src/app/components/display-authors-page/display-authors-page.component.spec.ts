import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayAuthorsPageComponent } from './display-authors-page.component';

describe('DisplayAuthorsPageComponent', () => {
  let component: DisplayAuthorsPageComponent;
  let fixture: ComponentFixture<DisplayAuthorsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayAuthorsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayAuthorsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
