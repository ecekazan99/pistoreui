import {Component, OnInit} from '@angular/core';
import { Author } from 'src/app/models/author.model';
import { AuthorServiceService } from 'src/app/services/author-service/author-service.service';
import {Router,ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-display-authors-page',
  templateUrl: './display-authors-page.component.html',
  styleUrls: ['./display-authors-page.component.scss']
})
export class DisplayAuthorsPageComponent implements OnInit {

  constructor(private routes: Router,private _Activatedroute:ActivatedRoute, private getAuthorService:AuthorServiceService) { }
  authorArray:Author[]=[];
  id : any;
  
  
  ngOnInit() {
    this.id=this._Activatedroute.snapshot.paramMap.get("id");

      this.getAuthorService.getAuthors().subscribe((data:any[])=>{
      this.authorArray=data;
    });
  }  
  
  displayAuthorDetail(id:any){
    this.routes.navigate(['/authorDetail',id]);
  } 

}
