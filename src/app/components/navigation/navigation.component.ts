import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(private routes: Router) { }

  ngOnInit(): void {
  }

  directHome(){
    this.routes.navigate(['']);
  }

  directAddAuthor(){
    this.routes.navigate(['/addAuthor']);
  }

  directLogin(){
    this.routes.navigate(['/login']);
  }

  directBook(){
    this.routes.navigate(['/addBook'])
  }

  directRegister(){
    this.routes.navigate(['/register']);
  }

  directBookShow(){
    this.routes.navigate(['/addBookShow'])
  }

  directAllAuthor(){
    this.routes.navigate(['/allAuthor']);
  }

  directAllBook(){
    this.routes.navigate(['/allBook']);
  }
}
